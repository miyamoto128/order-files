# order-files
Command line tool to order files by years and months.

## Description
Recursively browse regular files within source directory, and order them by modified time into numeric years and months destination directories.
The files last modification time is used to order the files.

The copied files keep the same access/modification time than the original files.

## Usage
```sh
Usage:
order source_dir dest_dir

source_dir      directory containing the regular files to be copied from and ordered
dest_dir        directory where the ordered files are copied into
```

### Example

Sample source directory containing 3 regular files:
```sh
$ ls -R -l --full-time samples/
samples/:
total 4
-rw-rw-r-- 1 noi noi    0 1999-01-01 00:00:00.000000000 +0100 file1.txt
-rw-rw-r-- 1 noi noi    0 1999-01-01 00:00:00.000000000 +0100 file2.txt
drwx------ 2 noi noi 4096 2023-03-29 17:31:21.551402753 +0200 folder1

samples/folder1:
total 0
-rw-rw-r-- 1 noi noi 0 2023-03-29 17:31:21.551402753 +0200 file3.txt
```

Execute order-files:
```sh
$ order samples/ result
INFO[0000] Process directory samples/                   
INFO[0000] Copy samples/file1.txt in result/1999/January/file1.txt 
INFO[0000] Copy samples/file2.txt in result/1999/January/file2.txt 
INFO[0000] Process directory samples/folder1            
INFO[0000] Copy samples/folder1/file3.txt in result/2023/March/file3.txt 

Processed 3 files in 2.547915ms
```

The source files have been copied and ordered within years and months destination folders:
```sh
$ tree result/
result/
├── 1999
│   └── 01
│       ├── file1.txt
│       └── file2.txt
└── 2023
    └── 03
        └── file3.txt

4 directories, 3 files
```

## Installation
Install go version >= 1.19.

From the project directory execute:
```sh
go build .
```

A fresh `order` executable, ready to use, must be created :)
Move in one of your favorite folders available in your PATH.


## License
GPL v3
