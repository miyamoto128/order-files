package core

import (
	"fmt"
	"io/fs"
	"order/utils"
	"os"
	"path/filepath"
	"strconv"
	"sync/atomic"
	"time"

	log "github.com/sirupsen/logrus"
)

type Orderer struct {
	srcDir     string
	dstDir     string
	processing atomic.Bool
	stats
}

type stats struct {
	lastDuration  time.Duration
	lastNbOrdered uint64
}

func NewOrderer(srcDir, dstDir string) *Orderer {
	return &Orderer{srcDir: srcDir, dstDir: dstDir}
}

func (o *Orderer) ProcessDir() {
	if !o.processing.CompareAndSwap(false, true) {
		log.Warnf("Aldready processing %s", o.srcDir)
		return
	}

	// Reset stats
	o.lastDuration = 0
	o.lastNbOrdered = 0

	start := time.Now()
	o.doProcessDir(o.srcDir)

	o.lastDuration = time.Since(start)
	o.processing.Store(false)
}

func (o Orderer) GetLastDuration() time.Duration {
	return o.lastDuration
}

func (o Orderer) GetLastNbOrderedFiles() uint64 {
	return o.lastNbOrdered
}

func (o *Orderer) doProcessDir(srcDir string) {
	entries, err := os.ReadDir(srcDir)
	if err != nil {
		log.Errorf("Can not access directory %s", srcDir)
	}

	log.Infof("Process directory %s", srcDir)
	for _, e := range entries {
		if e.IsDir() {
			nextSrcDir := filepath.Join(srcDir, e.Name())
			o.doProcessDir(nextSrcDir)
			continue
		}

		if err = o.orderByMonth(srcDir, e); err != nil {
			log.Errorf("Can not access file %s", srcDir)
			continue
		}

		o.lastNbOrdered++
	}
}

func (o Orderer) orderByMonth(srcDir string, entry fs.DirEntry) error {
	info, err := entry.Info()
	if err != nil {
		return err
	}

	fullDstDir, dstFileName := o.computeSafeDstPath(info.ModTime(), entry.Name())

	// Create destination directories
	log.Debugf("Create directory %s", fullDstDir)
	err = os.MkdirAll(fullDstDir, os.ModePerm)
	if err != nil {
		return err
	}

	srcPath := filepath.Join(srcDir, entry.Name())
	dstPath := filepath.Join(fullDstDir, dstFileName)

	// Copy file
	_, err = utils.Copy(srcPath, dstPath)
	if err != nil {
		log.Errorf("Could not copy %s in %s - %v", srcPath, dstPath, err)
		return err
	}

	log.Infof("Copied %s in %s", srcPath, dstPath)

	// Set source access/modification time
	os.Chtimes(dstPath, info.ModTime(), info.ModTime())
	if err != nil {
		log.Warnf("Could not change %s access/modification time - %v", dstPath, err)
	}

	return nil
}

func (o Orderer) computeSafeDstPath(modTime time.Time, filename string) (string, string) {
	year := strconv.Itoa(modTime.Year())
	month := fmt.Sprintf("%02d", modTime.Month())
	dstDir := filepath.Join(o.dstDir, year, month)

	// Safety: if current file already exists, prefix with underscore char
	dstFilename := filename
	for utils.Exists(filepath.Join(dstDir, dstFilename)) {
		dstFilename = fmt.Sprintf("_%s", dstFilename)
	}

	return dstDir, dstFilename
}
