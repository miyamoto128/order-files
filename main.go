package main

import (
	"flag"
	"fmt"
	"order/core"
	"os"

	log "github.com/sirupsen/logrus"
)

func init() {
	log.SetLevel(log.InfoLevel)
	log.SetOutput(os.Stdout)
}

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) != 2 {
		usage()
		log.Fatal("Bad arguments!")
	}

	srcDir, dstDir := args[0], args[1]

	o := core.NewOrderer(srcDir, dstDir)
	o.ProcessDir()

	fmt.Println()
	fmt.Printf("Processed %d files in %s", o.GetLastNbOrderedFiles(), o.GetLastDuration())
	fmt.Println()
}

func usage() {
	fmt.Println("Usage:")
	fmt.Println("order source_dir dest_dir")
	fmt.Println()
	fmt.Println("source_dir \tdirectory containing the regular files to be copied from and ordered")
	fmt.Println("dest_dir   \tdirectory where the ordered files are copied into")
	fmt.Println()
}
